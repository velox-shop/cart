
FROM eclipse-temurin:21.0.4_7-jdk-alpine
VOLUME /tmp
COPY ./build/libs/*.jar app.jar
ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS
ADD entrypoint.sh entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]