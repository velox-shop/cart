package shop.velox.cart.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;
import shop.velox.cart.AbstractIntegrationTest;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.dao.ItemRepository;
import shop.velox.cart.dao.MetadataEntryRepository;

class MetaDataEntityIntegrationTest extends AbstractIntegrationTest {

  @Nested
  class MetaDataValueCreationTest {

    @Resource
    MetadataEntryRepository metaDataEntryRepository;

    @BeforeEach
    void setUp() {
      metaDataEntryRepository.deleteAll();
    }

    @Test
    void usageTogetherTest() {

      // When
      metaDataEntryRepository.save(MetadataEntryEntity.builder()
          .key("key1")
          .values(List.of(
              MetadataValueEntity.builder().stringValue("value1").build(),
              MetadataValueEntity.builder().stringValue("value2").build()
          ))
          .build());

      // Then
      MetadataEntryEntity metaDataEntry = metaDataEntryRepository.findAll(Pageable.unpaged())
          .getContent()
          .getFirst();

      assertNotNull(metaDataEntry);
      assertThat(metaDataEntry.getValues()).hasSize(2);

    }

  }

  @Nested
  class MetaDataEntryCreationTest {

    @Resource
    MetadataEntryRepository metadataEntryRepository;

    @Resource
    CartRepository cartRepository;

    @Resource
    ItemRepository itemRepository;

    @BeforeEach
    @AfterEach
    void setUp() {
      itemRepository.deleteAll();
      cartRepository.deleteAll();
      metadataEntryRepository.deleteAll();
    }

    @Test
    void test() {
      metadataEntryRepository.save(MetadataEntryEntity.builder()
          .key("key1")
          .values(List.of(
              MetadataValueEntity.builder().stringValue("value1").build(),
              MetadataValueEntity.builder().stringValue("value2").build()
          )).build());

      metadataEntryRepository.save(MetadataEntryEntity.builder()
          .key("key2")
          .values(List.of(
              MetadataValueEntity.builder().stringValue("value3").build()
          )).build());

      var metadataEntries = metadataEntryRepository.findAll(Pageable.unpaged())
          .getContent();

      assertNotNull(metadataEntries);
      assertThat(metadataEntries).hasSize(2);
      assertThat(metadataEntries.get(0).getValues()).hasSize(2);
      assertThat(metadataEntries.get(1).getValues()).hasSize(1);
    }

    @Test
    void usageTogehterTest() {

      // When
      cartRepository.save(CartEntity.builder()
          .code("cart1")
          .metaData(MetaDataEntity.builder()
              .metadataEntries(
                  List.of(
                      MetadataEntryEntity.builder()
                          .key("key1")
                          .values(List.of(
                              MetadataValueEntity.builder().stringValue("value1").build(),
                              MetadataValueEntity.builder().stringValue("value2").build()
                          )).build(),
                      MetadataEntryEntity.builder()
                          .key("key2")
                          .values(List.of(
                              MetadataValueEntity.builder().stringValue("value3").build()
                          )).build()
                  ))
              .build())
          .build());

      // Then
      CartEntity cart = cartRepository.findByCode("cart1").orElseThrow();
      MetaDataEntity cartMetaData = cart.getMetaData();
      assertThat(cartMetaData.getMetadataEntries()).hasSize(2);
      assertThat(cartMetaData.getMetadataEntries().get(0).getValues()).hasSize(2);
      assertThat(cartMetaData.getMetadataEntries().get(1).getValues()).hasSize(1);

    }

    @Test
    void multipleEntitiesTest() {

      // When

      // There is no cascade on items, so we need to save the cart first
      var cartSaved = cartRepository.save(CartEntity.builder()
          .code("cart2")
          .metaData(MetaDataEntity.builder()
              .metadataEntries(List.of(
                  MetadataEntryEntity.builder()
                      .key("key1")
                      .values(List.of(
                          MetadataValueEntity.builder().stringValue("value1").build(),
                          MetadataValueEntity.builder().stringValue("value2").build()
                      )).build(),
                  MetadataEntryEntity.builder()
                      .key("key2")
                      .values(List.of(
                          MetadataValueEntity.builder().stringValue("value3").build()
                      )).build()
              ))
              .build())
          .build());

      itemRepository.save(ItemEntity.builder()
          .id("item2_1")
          .productCode("article1")
          .cart(cartSaved)
          .quantity(BigDecimal.ONE)
          .metaData(MetaDataEntity.builder()
              .metadataEntries(List.of(
                  MetadataEntryEntity.builder()
                      .key("itemKey1")
                      .values(List.of(
                          MetadataValueEntity.builder().stringValue("itemValue1").build(),
                          MetadataValueEntity.builder().stringValue("itemValue2").build()
                      )).build(),
                  MetadataEntryEntity.builder()
                      .key("itemKey2")
                      .values(List.of(
                          MetadataValueEntity.builder().stringValue("itemValue3").build()
                      )).build()
              ))
              .build())
          .build());

      // Then
      CartEntity cart = cartRepository.findByCode("cart2").orElseThrow();

      MetaDataEntity cartMetaData = cart.getMetaData();
      assertNotNull(cartMetaData);
      assertThat(cartMetaData.getMetadataEntries()).hasSize(2);
      assertThat(cartMetaData.getMetadataEntries().get(0).getValues()).hasSize(2);
      assertThat(cartMetaData.getMetadataEntries().get(1).getValues()).hasSize(1);

      ItemEntity itemEntity = itemRepository.findItemByCartCodeAndId("cart2", "item2_1")
          .orElseThrow();
      assertThat(itemEntity.getMetaData().getMetadataEntries()).hasSize(2);
    }

  }

}
