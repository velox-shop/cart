package shop.velox.cart;

import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;

import jakarta.annotation.Resource;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.TestPropertySource;
import shop.velox.cart.api.controller.client.CartApiClient;
import shop.velox.cart.api.controller.client.ItemApiClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {
    "logging.level.org.springframework.web.client=TRACE",
})
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  private final Logger log = LoggerFactory.getLogger(AbstractWebIntegrationTest.class);

  public static final String HOST_URL = "http://localhost:%s";

  @Resource
  RestTemplateBuilder restTemplateBuilder;

  @Value(value = "${local.server.port}")
  protected int port;


  @Getter(AccessLevel.PROTECTED)
  private CartApiClient anonymousCartApiClient;

  @Getter(AccessLevel.PROTECTED)
  private CartApiClient user1CartApiClient;

  @Getter(AccessLevel.PROTECTED)
  private CartApiClient user2CartApiClient;

  @Getter(AccessLevel.PROTECTED)
  private CartApiClient adminCartApiClient;


  @Getter(AccessLevel.PROTECTED)
  private ItemApiClient anonymousItemApiClient;

  @Getter(AccessLevel.PROTECTED)
  private ItemApiClient user1ItemApiClient;

  @Getter(AccessLevel.PROTECTED)
  private ItemApiClient user2ItemApiClient;

  @Getter(AccessLevel.PROTECTED)
  private ItemApiClient adminItemApiClient;


  @BeforeEach
  public void setup() throws InterruptedException {
    log.info("Web test method setup");

    anonymousCartApiClient = new CartApiClient(
        restTemplateBuilder.build(), String.format(HOST_URL, port));

    user1CartApiClient = new CartApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_1_USERNAME, LOCAL_USER_1_PASSWORD)
            .build(), String.format(HOST_URL, port));

    user2CartApiClient = new CartApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_2_USERNAME, LOCAL_USER_2_PASSWORD)
            .build(), String.format(HOST_URL, port));

    adminCartApiClient = new CartApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD)
            .build(), String.format(HOST_URL, port));

    anonymousItemApiClient = new ItemApiClient(
        restTemplateBuilder.build(), String.format(HOST_URL, port));

    user1ItemApiClient = new ItemApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_1_USERNAME, LOCAL_USER_1_PASSWORD)
            .build(), String.format(HOST_URL, port));

    user2ItemApiClient = new ItemApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_USER_2_USERNAME, LOCAL_USER_2_PASSWORD)
            .build(), String.format(HOST_URL, port));

    adminItemApiClient = new ItemApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD)
            .build(), String.format(HOST_URL, port));
  }

}
