package shop.velox.cart.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import jakarta.annotation.Resource;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import shop.velox.cart.AbstractIntegrationTest;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.model.CartEntity;

class CartServiceIntegrationTest extends AbstractIntegrationTest {

  @BeforeEach
  void setup() {
    cartRepository.deleteAll();
  }

  @Resource
  CartService cartService;

  @Resource
  CartRepository cartRepository;

  @Nested
  class CreateCart {

    @Test
    @WithAnonymousUser
    void createAnonymousCartOKTest() {
      CartDto cartDto = cartService.createCart(
          CreateCartDto.builder()
              .currencyIsoCode("CHF")
              .build(),
          null);
      assertThat(cartDto).isNotNull();
      assertThat(cartDto.getUserId()).isNull();
    }

    @Test
    @WithMockUser(username = "aUserid")
    void createOwnCartOKTest() {
      CartDto cartDto = cartService.createCart(
          CreateCartDto.builder()
              .currencyIsoCode("CHF")
              .build(),
          "aUserid");
      assertThat(cartDto).isNotNull();
      assertThat(cartDto.getUserId()).isEqualTo("aUserid");
    }

  }

  @Nested
  class GetCartTest {

    @Test
    @WithAnonymousUser
    void getAnonymousCartOKTest() {
      cartRepository.save(CartEntity.builder()
          .code("123")
          .currencyIsoCode("CHF")
          .build());

      Optional<CartDto> cartDtoOptional = cartService.getCart("123");
      assertThat(cartDtoOptional).isPresent();
    }

    @Test
    @WithAnonymousUser
    void getAnonymousCartForbiddenTest() {
      cartRepository.save(CartEntity.builder()
          .code("123")
          .userId("aUserid")
          .currencyIsoCode("CHF")
          .build());

      assertThrows(AccessDeniedException.class, () -> cartService.getCart("123"));
    }

    @Test
    @WithMockUser(username = "aUserid")
    void getOwnCartOKTest() {
      cartRepository.save(CartEntity.builder()
          .code("123")
          .userId("aUserid")
          .currencyIsoCode("CHF")
          .build());

      Optional<CartDto> cartDtoOptional = cartService.getCart("123");
      assertThat(cartDtoOptional).isPresent();
    }

    @Test
    @WithMockUser(username = "aUserid")
    void getOthersCartForbiddenTest() {
      cartRepository.save(CartEntity.builder()
          .code("123")
          .userId("differentUserid")
          .currencyIsoCode("CHF")
          .build());

      assertThrows(AccessDeniedException.class, () -> cartService.getCart("123"));
    }

    @Test
    @WithMockUser(username = "aUserid", authorities = {"Admin"})
    void getOthersCartAsAdminOkTest() {
      cartRepository.save(CartEntity.builder()
          .code("123")
          .userId("differentUserid")
          .currencyIsoCode("CHF")
          .build());

      Optional<CartDto> cartDtoOptional = cartService.getCart("123");
      assertThat(cartDtoOptional).isPresent();
    }

  }

}
