package shop.velox.cart.permissions;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static shop.velox.cart.service.CartServiceConstants.Authorities.CART_ADMIN;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.commons.security.service.impl.AuthorizationEvaluatorImpl;

class CartAuthorizationEvaluatorTest {

  AuthorizationEvaluatorImpl authorizationEvaluator = new AuthorizationEvaluatorImpl();
  CartAuthorizationEvaluator cartAuthorizationEvaluator = new CartAuthorizationEvaluator(
      authorizationEvaluator, null, null);

  public static Stream<Arguments> canAccessUserWithCodeTestArguments() {
    return Stream.of(
        Arguments.of(null, null, List.of(), true),
        Arguments.of("userId", null, List.of(), false),
        Arguments.of(null, "userId", List.of("User"), false),
        Arguments.of("userId", "userId", List.of("User"), true),
        Arguments.of("userId", "otherUserId", List.of("Admin"), true),
        Arguments.of("userId", "otherUserId", List.of(CART_ADMIN), true),
        Arguments.of("userId", "otherUserId", List.of("User"), false)
    );
  }

  @ParameterizedTest
  @MethodSource("canAccessUserWithCodeTestArguments")
  void canAccessCartTest(String cartUserId, String authenticationUserName, List<String> authorities,
      boolean shouldBeAllowed) {
    // Given
    CartDto user = CartDto.builder()
        .userId(cartUserId)
        .build();

    Authentication authentication = new UsernamePasswordAuthenticationToken(
        authenticationUserName,
        null,
        authorities.stream()
            .map(SimpleGrantedAuthority::new)
            .toList());

    // When
    var isAllowed = cartAuthorizationEvaluator.canAccessCart(authentication,
        isEmpty(authenticationUserName), Optional.of(user));

    // Then
    assertEquals(shouldBeAllowed, isAllowed);
  }


}
