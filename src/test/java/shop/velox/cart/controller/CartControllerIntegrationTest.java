package shop.velox.cart.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.cart.model.enums.CartType.SHOPPING_CART;
import static shop.velox.cart.model.enums.CartType.WISHLIST;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;

import jakarta.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import shop.velox.cart.AbstractWebIntegrationTest;
import shop.velox.cart.api.controller.client.CartApiClient;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.api.dto.metadata.MetaDataDto;
import shop.velox.cart.api.dto.metadata.MetaDataEntryDto;
import shop.velox.cart.api.dto.metadata.MetaDataValueDto;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.enums.CartType;


public class CartControllerIntegrationTest extends AbstractWebIntegrationTest {

  static CartDto createCartDto(CartApiClient cartApiClient) {
    var cartToCreate = CreateCartDto.builder()
        .currencyIsoCode("USD")
        .name("test wishlist")
        .type(CartType.WISHLIST)
        .build();
    var responseEntity = cartApiClient.createCart(cartToCreate);
    assertEquals(CREATED, responseEntity.getStatusCode());
    CartDto createdCart = responseEntity.getBody();
    assertNotNull(createdCart);
    assertEquals(cartToCreate.getCurrencyIsoCode(), createdCart.getCurrencyIsoCode());
    assertEquals(cartToCreate.getName(), createdCart.getName());
    assertEquals(cartToCreate.getType(), createdCart.getType());
    return createdCart;
  }

  @Nested
  class CreateCartTest {

    @Nested
    class MetaDataTest {

      @Test
      void createCart() {
        CreateCartDto createCartDto = CreateCartDto.builder()
            .currencyIsoCode("USD")
            .metaData(MetaDataDto.builder().metadataEntries(List.of(
                MetaDataEntryDto.builder()
                    .key("key1")
                    .values(List.of(MetaDataValueDto.builder().stringValue("stringValue1").build()))
                    .build(),
                MetaDataEntryDto.builder()
                    .key("key2")
                    .values(List.of(
                        MetaDataValueDto.builder().numberValue(BigDecimal.ONE).build()))
                    .build()
            )).build()).build();
        var responseEntity = getAnonymousCartApiClient().createCart(createCartDto);
        assertEquals(CREATED, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        MetaDataDto metaData = body.getMetaData();
        assertNotNull(metaData);
        assertNotNull(metaData.getMetadataEntries());
        assertEquals(2, metaData.getMetadataEntries().size());

        MetaDataEntryDto metaDataEntryDto1 = metaData.getMetadataEntries().get(0);
        assertEquals("key1", metaDataEntryDto1.getKey());
        assertEquals("stringValue1",
            metaDataEntryDto1.getValues().getFirst().getStringValue());

        MetaDataEntryDto metaDataEntryDto2 = metaData.getMetadataEntries().get(1);
        assertEquals("key2", metaDataEntryDto2.getKey());
        assertEquals(BigDecimal.ONE,
            metaDataEntryDto2.getValues().getFirst().getNumberValue());
      }

    }

    @Nested
    @TestPropertySource(properties = {
        "logging.level.org.springframework.security=TRACE"
    })
    class PermissionsTest {

      private CreateCartDto createCartDto() {
        return CreateCartDto.builder()
            .currencyIsoCode("USD")
            .build();
      }

      @Test
      void anonymousCanCreateCart() {
        CreateCartDto createCartDto = createCartDto();
        var responseEntity = getAnonymousCartApiClient().createCart(createCartDto);
        assertEquals(CREATED, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(createCartDto.getCurrencyIsoCode(), body.getCurrencyIsoCode());
        assertNull(body.getUserId());
      }

      @Test
      void userCanCreateCart() {
        CreateCartDto createCartDto = createCartDto();
        var responseEntity = getUser1CartApiClient().createCart(createCartDto);
        assertEquals(CREATED, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(createCartDto.getCurrencyIsoCode(), body.getCurrencyIsoCode());
        assertEquals(LOCAL_USER_1_USERNAME, body.getUserId());
      }

      @Test
      void adminCanCreateCart() {
        CreateCartDto createCartDto = createCartDto();
        var responseEntity = getAdminCartApiClient().createCart(createCartDto);
        assertEquals(CREATED, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(createCartDto.getCurrencyIsoCode(), body.getCurrencyIsoCode());
        assertEquals(LOCAL_ADMIN_USERNAME, body.getUserId());
      }

    }

  }

  @Nested
  class UpdateCartTest {

    @Nested
    class MetaDataTest {

      @Test
      void updatesCartWithMetaData() {
        CartApiClient apiClient = getAnonymousCartApiClient();
        CartDto cartDto = createCartDto(apiClient);
        String cartCode = cartDto.getCode();

        MetaDataDto metaData = MetaDataDto.builder().metadataEntries(List.of(
                MetaDataEntryDto.builder()
                    .key("key1")
                    .values(List.of(MetaDataValueDto.builder().stringValue("stringValue1").build()))
                    .build(),
                MetaDataEntryDto.builder()
                    .key("key2")
                    .values(List.of(
                        MetaDataValueDto.builder().numberValue(BigDecimal.ONE).build()))
                    .build()
            ))
            .build();
        UpdateCartDto payload = UpdateCartDto.builder()
            .currencyIsoCode("EUR")
            .metaData(metaData)
            .build();
        var responseEntity = apiClient.updateCart(cartCode, payload);
        assertEquals(OK, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(payload.getCurrencyIsoCode(), body.getCurrencyIsoCode());
        assertEquals(metaData, body.getMetaData());
      }

    }


    @Nested
    @TestPropertySource(properties = {
        "logging.level.org.springframework.security=TRACE"
    })
    class PermissionsTest {

      @Test
      void anonymousUpdatesOwnCart() {
        CartApiClient apiClient = getAnonymousCartApiClient();
        CartDto cartDto = createCartDto(apiClient);
        String cartCode = cartDto.getCode();

        UpdateCartDto payload = UpdateCartDto.builder()
            .currencyIsoCode("EUR")
            .build();
        var responseEntity = apiClient.updateCart(cartCode, payload);
        assertEquals(OK, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(payload.getCurrencyIsoCode(), body.getCurrencyIsoCode());
      }

      @Test
      void userUpdatesOwnCart() {
        CartApiClient apiClient = getUser1CartApiClient();
        CartDto cartDto = createCartDto(apiClient);
        String cartCode = cartDto.getCode();

        UpdateCartDto payload = UpdateCartDto.builder()
            .currencyIsoCode("EUR")
            .build();
        var responseEntity = apiClient.updateCart(cartCode, payload);
        assertEquals(OK, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(payload.getCurrencyIsoCode(), body.getCurrencyIsoCode());
      }

      @Test
      void userUpdatesOtherUserCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        UpdateCartDto payload = UpdateCartDto.builder()
            .currencyIsoCode("EUR")
            .build();

        assertThrows(Forbidden.class,
            () -> getUser2CartApiClient().updateCart(cartCode, payload));
      }

      @Test
      void userTakesOverAnonymousCart() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        assertNull(cartDto.getUserId());

        UpdateCartDto payload = UpdateCartDto.builder()
            .currencyIsoCode("EUR")
            .build();
        var responseEntity = getUser1CartApiClient().updateCart(cartCode, payload);
        assertEquals(OK, responseEntity.getStatusCode());
        CartDto body = responseEntity.getBody();
        assertNotNull(body);
        assertEquals(payload.getCurrencyIsoCode(), body.getCurrencyIsoCode());
        assertEquals(LOCAL_USER_1_USERNAME, body.getUserId());


      }

    }

  }

  @Nested
  class GetCartTest {

    @Nested
    class StatusCode {

      @Test
      void anonymousGetsNotExistingCartTest() {
        assertThrows(NotFound.class,
            () -> getAnonymousCartApiClient().getCart("not-existing"));
      }

      @Test
      void userGetsNotExistingCartTest() {
        assertThrows(NotFound.class,
            () -> getUser1CartApiClient().getCart("not-existing"));
      }

      @Test
      void adminGetsNotExistingCartTest() {
        assertThrows(NotFound.class,
            () -> getAdminCartApiClient().getCart("not-existing"));
      }
    }

    @Nested
    @TestPropertySource(properties = {
        "logging.level.org.springframework.security=TRACE"
    })
    class Permissions {

      @Test
      void anonymousGetsOwnCart() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();

        var responseEntity = getAnonymousCartApiClient().getCart(cartCode);

        assertEquals(OK, responseEntity.getStatusCode());
      }

      @Test
      void userGetsOwnCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        var responseEntity = getUser1CartApiClient().getCart(cartCode);

        assertEquals(OK, responseEntity.getStatusCode());
      }

      @Test
      void userGetsOtherUserCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        assertThrows(Forbidden.class,
            () -> getUser2CartApiClient().getCart(cartCode));
      }
    }
  }

  @Nested
  class GetAllCarts {


    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class Usage {

      @Resource
      CartRepository cartRepository;


      @BeforeAll
      void setup() {
        cartRepository.deleteAll();
        
        cartRepository.save(CartEntity.builder()
            .userId(LOCAL_USER_1_USERNAME)
            .currencyIsoCode("CHF")
            .type(SHOPPING_CART)
            .build());

        cartRepository.save(CartEntity.builder()
            .userId(LOCAL_USER_1_USERNAME)
            .currencyIsoCode("CHF")
            .type(WISHLIST)
            .build());

        cartRepository.save(CartEntity.builder()
            .userId(LOCAL_USER_2_USERNAME)
            .currencyIsoCode("CHF")
            .type(SHOPPING_CART)
            .build());
      }

      @AfterAll
      void tearDown() {
        cartRepository.deleteAll();
      }

      @Test
      void noFilterTest() {
        var responseEntity = getAdminCartApiClient().getAllCarts(null, null,
            Pageable.ofSize(10));
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().getTotalElements(), 3);
      }

      @Test
      void cartTypeFilterTest() {
        var responseEntity = getAdminCartApiClient().getAllCarts(null, SHOPPING_CART,
            Pageable.ofSize(10));
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().getTotalElements(), 2);
      }

      @Test
      void userIdFilterTest() {
        var responseEntity = getAdminCartApiClient().getAllCarts(LOCAL_USER_1_USERNAME, null,
            Pageable.ofSize(10));
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().getTotalElements(), 2);
      }

      @Test
      void userIdAndCartTypeFilterTest() {
        var responseEntity = getAdminCartApiClient().getAllCarts(LOCAL_USER_1_USERNAME,
            SHOPPING_CART, Pageable.ofSize(10));
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().getTotalElements(), 1);
      }
    }

    @Nested
    class Permissions {

      @Test
      void anonymousGetsAllCarts() {
        assertThrows(Forbidden.class,
            () -> getAnonymousCartApiClient().getAllCarts(Pageable.ofSize(10)));
      }

      @Test
      void userGetsAllCarts() {
        assertThrows(Forbidden.class,
            () -> getUser1CartApiClient().getAllCarts(Pageable.ofSize(10)));
      }

      @Test
      void userGetsAllUserCarts() {
        CartDto cart = createCartDto(getUser1CartApiClient());

        var responseEntity = getUser1CartApiClient().getAllCarts(cart.getUserId(), null,
            Pageable.ofSize(10));

        assertEquals(OK, responseEntity.getStatusCode());
        Page<CartDto> page = responseEntity.getBody();
        assertNotNull(page);
      }

      @Test
      void adminGetsAllCarts() {
        var responseEntity = getAdminCartApiClient().getAllCarts(Pageable.ofSize(10));
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
      }
    }
  }

  @Nested
  class DeleteCart {

    @Nested
    class Permissions {

      @Test
      void anonymousDeletesOwnCart() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();

        var responseEntity = getAnonymousCartApiClient().deleteCart(cartCode);

        assertEquals(NO_CONTENT, responseEntity.getStatusCode());
      }

      @Test
      void userDeletesOwnCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        var responseEntity = getUser1CartApiClient().deleteCart(cartCode);

        assertEquals(NO_CONTENT, responseEntity.getStatusCode());
      }

      @Test
      void userDeletesOtherUserCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        assertThrows(Forbidden.class,
            () -> getUser2CartApiClient().deleteCart(cartCode));
      }

      @Test
      void adminDeletesUserCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();

        var responseEntity = getAdminCartApiClient().deleteCart(cartCode);

        assertEquals(NO_CONTENT, responseEntity.getStatusCode());
      }
    }
  }
}
