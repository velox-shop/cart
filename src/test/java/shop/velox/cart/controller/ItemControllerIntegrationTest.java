package shop.velox.cart.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static shop.velox.cart.controller.CartControllerIntegrationTest.createCartDto;

import java.math.BigDecimal;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException.Conflict;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.client.HttpClientErrorException.NotFound;
import org.springframework.web.client.HttpClientErrorException.UnprocessableEntity;
import shop.velox.cart.AbstractWebIntegrationTest;
import shop.velox.cart.api.controller.client.ItemApiClient;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;

public class ItemControllerIntegrationTest extends AbstractWebIntegrationTest {

  public static CartDto createOkItem(String cartCode, ItemApiClient itemApiClient) {
    var responseEntity = createItem(cartCode, itemApiClient);
    assertEquals(CREATED, responseEntity.getStatusCode());
    CartDto result = responseEntity.getBody();
    assertNotNull(result);
    assertThat(result.getItems()).hasSize(1);
    return result;
  }

  private static ResponseEntity<CartDto> createItem(String cartCode,
      ItemApiClient itemApiClient) {
    var responseEntity = itemApiClient.createItem(cartCode,
        CreateItemDto.builder()
            .productCode("123")
            .quantity(BigDecimal.ONE)
            .build());
    return responseEntity;
  }

  @Nested
  class GetItem {

    @Nested
    class Permissions {

      @Test
      void anonymousGetsOwnItem() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
        String itemId = cartWithItem.getItems().get(0).getId();

        assertNotNull(getAnonymousItemApiClient().getItem(cartCode, itemId));
      }

      @Test
      void userGetsOwnItem() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
        String itemId = cartWithItem.getItems().get(0).getId();

        assertNotNull(getUser1ItemApiClient().getItem(cartCode, itemId));
      }

      @Test
      void userGetsOtherUserItem() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
        String itemId = cartWithItem.getItems().get(0).getId();

        assertThrows(Forbidden.class,
            () -> getUser2ItemApiClient().getItem(cartCode, itemId));
      }

      @Test
      void adminGetsAnyItem() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
        String itemId = cartWithItem.getItems().get(0).getId();

        assertNotNull(getAdminItemApiClient().getItem(cartCode, itemId));
      }
    }
  }

  @Nested
  class CreateItem {

    @Nested
    class StatusCode {

      @Test
      void validItem() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        createOkItem(cartCode, getAnonymousItemApiClient());
      }

      @Test
      void createItemInNotExistingCart() {
        assertThrows(NotFound.class,
            () -> createItem("NotExisting", getAnonymousItemApiClient()));
      }

      @Test
      void invalidItem() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();

        CreateItemDto createItemDto = CreateItemDto.builder()
            .productCode(null)
            .quantity(null)
            .build();
        assertThrows(UnprocessableEntity.class,
            () -> getAnonymousItemApiClient().createItem(cartCode, createItemDto));
      }

      @Test
      void create2ItemsWithSameProduct() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        createOkItem(cartCode, getAnonymousItemApiClient());

        assertThrows(Conflict.class,
            () -> createItem(cartCode, getAnonymousItemApiClient()));
      }

    }


    @Nested
    class Permissions {

      @Test
      void anonymousAddsToOwnCart() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        createOkItem(cartCode, getAnonymousItemApiClient());
      }

      @Test
      void userAddsToOwnCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();
        createOkItem(cartCode, getUser1ItemApiClient());
      }

      @Test
      void userAddsToOtherUserCart() {
        CartDto cartDto = createCartDto(getUser1CartApiClient());
        String cartCode = cartDto.getCode();
        assertThrows(Forbidden.class,
            () -> createItem(cartCode, getUser2ItemApiClient()));
      }

    }

  }

  @Nested
  class UpdateItem {

    @Nested
    class Behaviour {

      @Test
      void updateItemWithPositiveQuantity() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
        ItemDto itemDto = cartWithItem.getItems().get(0);
        String itemId = itemDto.getId();

        UpdateItemDto payload = UpdateItemDto.builder()
            .quantity(BigDecimal.TEN)
            .build();
        var responseEntity = getAnonymousItemApiClient().updateItem(cartCode, itemId,
            payload);
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        List<ItemDto> items = responseEntity.getBody().getItems();
        assertThat(items).hasSize(1);
        assertEquals(BigDecimal.TEN, items.get(0).getQuantity());
      }

      @Test
      void updateItemWithZeroQuantity() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
        ItemDto itemDto = cartWithItem.getItems().get(0);
        String itemId = itemDto.getId();

        UpdateItemDto payload = UpdateItemDto.builder()
            .quantity(BigDecimal.ZERO)
            .build();
        var responseEntity = getAnonymousItemApiClient().updateItem(cartCode, itemId,
            payload);
        assertEquals(OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        List<ItemDto> items = responseEntity.getBody().getItems();
        assertThat(items).isEmpty();
      }

      @Nested
      class Permissions {

        @Test
        void anonymousUpdatesOwnItem() {
          CartDto cartDto = createCartDto(getAnonymousCartApiClient());
          String cartCode = cartDto.getCode();
          CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
          ItemDto itemDto = cartWithItem.getItems().get(0);
          String itemId = itemDto.getId();

          UpdateItemDto payload = UpdateItemDto.builder()
              .quantity(BigDecimal.TEN)
              .build();
          var responseEntity = getAnonymousItemApiClient().updateItem(cartCode, itemId,
              payload);
          assertEquals(OK, responseEntity.getStatusCode());
          assertNotNull(responseEntity.getBody());
        }

      }

    }

  }

  @Nested
  class DeleteItem {

    @Nested
    class Behaviour {

      @Test
      void deleteItem() {
        CartDto cartDto = createCartDto(getAnonymousCartApiClient());
        String cartCode = cartDto.getCode();
        CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
        ItemDto itemDto = cartWithItem.getItems().get(0);
        String itemId = itemDto.getId();

        var responseEntity = getAnonymousItemApiClient().deleteItem(cartCode, itemId);
        assertEquals(OK, responseEntity.getStatusCode());
        CartDto updatedCart = responseEntity.getBody();
        assertNotNull(updatedCart);
        assertThat(updatedCart.getItems()).isEmpty();
      }

      @Nested
      class Permissions {

        @Test
        void anonymousDeletesOwnItem() {
          CartDto cartDto = createCartDto(getAnonymousCartApiClient());
          String cartCode = cartDto.getCode();
          CartDto cartWithItem = createOkItem(cartCode, getAnonymousItemApiClient());
          ItemDto itemDto = cartWithItem.getItems().get(0);
          String itemId = itemDto.getId();

          getAnonymousItemApiClient().deleteItem(cartCode, itemId);
        }

        @Test
        void userDeletesOwnItem() {
          CartDto cartDto = createCartDto(getUser1CartApiClient());
          String cartCode = cartDto.getCode();
          CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
          ItemDto itemDto = cartWithItem.getItems().get(0);
          String itemId = itemDto.getId();

          getUser1ItemApiClient().deleteItem(cartCode, itemId);
        }

        @Test
        void userDeletesOtherUserItem() {
          CartDto cartDto = createCartDto(getUser1CartApiClient());
          String cartCode = cartDto.getCode();
          CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
          ItemDto itemDto = cartWithItem.getItems().get(0);
          String itemId = itemDto.getId();

          assertThrows(Forbidden.class,
              () -> getUser2ItemApiClient().deleteItem(cartCode, itemId));
        }

        @Test
        void adminDeletesAnyItem() {
          CartDto cartDto = createCartDto(getUser1CartApiClient());
          String cartCode = cartDto.getCode();
          CartDto cartWithItem = createOkItem(cartCode, getUser1ItemApiClient());
          ItemDto itemDto = cartWithItem.getItems().get(0);
          String itemId = itemDto.getId();

          getAdminItemApiClient().deleteItem(cartCode, itemId);
        }
      }
    }

  }

}
