package shop.velox.cart.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.model.enums.CartType;


@Tag(name = "Cart", description = "the Cart API")
@RequestMapping(value = "/carts", produces = MediaType.APPLICATION_JSON_VALUE)
public interface CartController {

  @Operation(summary = "create new Cart")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "Cart created",
          content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  ResponseEntity<CartDto> createCart(
      @Parameter(description = "The cart to create.")
      @Valid @RequestBody final CreateCartDto cart
  );


  @Operation(summary = "Find Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "cart not found",
          content = @Content(schema = @Schema()))
  })
  @GetMapping("/{code}")
  ResponseEntity<CartDto> getCart(
      @Parameter(description = "Code of the Cart. Cannot be empty.", required = true)
      @PathVariable("code") final String cartCode);


  @Operation(summary = "Delete Cart by code", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "404", description = "cart not found")
  })
  @DeleteMapping(value = "/{code}")
  ResponseEntity<Void> removeCart(
      @Parameter(description = "Code of the Cart. Cannot be empty.", required = true)
      @PathVariable("code") final String id);


  @Operation(summary = "gets all carts. Paginated.", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = CartDto.class)))
  })
  @GetMapping
  Page<CartDto> getCarts(
      @Parameter(description = "Optional Filter on the userId of the carts")
      @RequestParam(value = "userId", required = false) @Nullable String userId,

      @Parameter(description = "Optional Filter on the Type of the carts")
      @RequestParam(value = "type", required = false) @Nullable CartType type,

      Pageable pageable);


  @Operation(summary = "Update a cart. It assigns anonymous cart to current user", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "cart not found",
          content = @Content(schema = @Schema()))
  })
  @PatchMapping(value = "/{code}")
  ResponseEntity<CartDto> updateCart(
      @Parameter(description = "Code of the Cart. Cannot be empty.", required = true)
      @PathVariable("code") final String cartCode,

      @Parameter(description = "The cart to update.", required = true)
      @RequestBody final UpdateCartDto updateCartDto);
}
