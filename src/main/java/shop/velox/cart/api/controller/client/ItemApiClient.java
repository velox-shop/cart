package shop.velox.cart.api.controller.client;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;

@Slf4j
public class ItemApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public ItemApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/cart/v1/carts/{cartCode}/items";
  }

  public ResponseEntity<ItemDto> getItem(String cartCode, String itemId) {
    Map<String, ? extends Serializable> uriPathParams = Map.of("cartCode", cartCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(itemId)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getItem with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, ItemDto.class);
  }

  public ResponseEntity<CartDto> createItem(String cartCode, CreateItemDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateItemDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("cartCode", cartCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createItem with URI: {} and payload: {}", uri, payload);

    return restTemplate.exchange(uri, POST, httpEntity, CartDto.class);
  }

  public ResponseEntity<CartDto> updateItem(String cartCode, String itemId, UpdateItemDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateItemDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("cartCode", cartCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(itemId)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("updateItem with URI: {}, payload: {}", uri, payload);

    return restTemplate.exchange(uri, PATCH, httpEntity, CartDto.class);
  }

  public ResponseEntity<CartDto> deleteItem(String cartCode, String itemId) {

    Map<String, ? extends Serializable> uriPathParams = Map.of("cartCode", cartCode);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(itemId)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("deleteItem with URI: {}", uri);

    return restTemplate.exchange(uri, DELETE, null/*httpEntity*/, CartDto.class);
  }

}
