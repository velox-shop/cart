package shop.velox.cart.api.controller.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.cart.api.controller.ItemController;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;
import shop.velox.cart.model.ItemEntity;
import shop.velox.cart.service.ItemService;
import shop.velox.commons.converter.Converter;


@RestController
@Slf4j
@RequiredArgsConstructor
public class ItemControllerImpl implements ItemController {

  private final ItemService itemService;

  private final Converter<ItemEntity, ItemDto> itemConverter;


  @Override
  public ResponseEntity<ItemDto> getItem(String cartCode, String itemId) {
    return itemService.getItem(cartCode, itemId)
        .map(itemDto -> new ResponseEntity<>(itemDto, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @Override
  public ResponseEntity<CartDto> addItem(String cartCode, CreateItemDto item) {
    return itemService.addItem(cartCode, item);
  }

  @Override
  public ResponseEntity<CartDto> updateItem(String cartCode, String itemId, UpdateItemDto item) {
    return itemService.updateItem(cartCode, itemId, item);
  }

  @Override
  public ResponseEntity<CartDto> removeItem(String cartCode, String itemId) {
    return itemService.removeItem(cartCode, itemId);
  }
}
