package shop.velox.cart.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;


@Tag(name = "Item", description = "the Item API")
@RequestMapping("/carts/{cartCode}/items")
public interface ItemController {

  @Operation(summary = "Gets an item by cartId and itemId")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = ItemDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found",
          content = @Content(schema = @Schema())),
  })
  @GetMapping(value = "/{itemId}")
  ResponseEntity<ItemDto> getItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true)
      @PathVariable("cartCode") final String cartCode,

      @Parameter(description = "Id of the item. Cannot be empty.", required = true)
      @PathVariable("itemId") final String itemId);


  @Operation(summary = "Adds an item to the cart")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Item was already existing, quantity was updated",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "201", description = "Item Created",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "cart not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409", description = "an item with given productCode already exists",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "payload is not valid",
          content = @Content(schema = @Schema())),
  })
  @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  ResponseEntity<CartDto> addItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true)
      @PathVariable("cartCode") final String cartCode,

      @Parameter(description = "Item to create. Cannot be empty.", required = true)
      @RequestBody @Valid final CreateItemDto item);


  @Operation(summary = "Updates an item in the cart")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory data missing",
          content = @Content(schema = @Schema()))
  })
  @PatchMapping(value = "{itemId}", consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<CartDto> updateItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true)
      @PathVariable("cartCode") final String cartCode,

      @Parameter(description = "Id of the item. Cannot be empty.", required = true)
      @PathVariable("itemId") final String itemId,

      @Parameter(description = "Item to update. Cannot be empty.", required = true)
      @RequestBody @Valid final UpdateItemDto item);


  @Operation(summary = "Removes an item from the cart")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation",
          content = @Content(schema = @Schema(implementation = CartDto.class))),
      @ApiResponse(responseCode = "404", description = "item not found",
          content = @Content(schema = @Schema()))
  })
  @DeleteMapping(value = "/{itemId}")
  ResponseEntity<CartDto> removeItem(
      @Parameter(description = "Id of the Cart. Cannot be empty.", required = true)
      @PathVariable("cartCode") final String cartCode,

      @Parameter(description = "Id of the item. Cannot be empty.", required = true)
      @PathVariable("itemId") final String itemId);
}
