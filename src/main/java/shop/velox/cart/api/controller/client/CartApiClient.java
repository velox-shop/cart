package shop.velox.cart.api.controller.client;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;

import java.io.Serializable;
import java.net.URI;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.model.enums.CartType;
import shop.velox.commons.rest.response.RestResponsePage;

@Slf4j
public class CartApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public CartApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/cart/v1/carts";
  }

  public ResponseEntity<CartDto> createCart(CreateCartDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateCartDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createCart with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, CartDto.class);
  }

  public ResponseEntity<CartDto> updateCart(String cartCode, UpdateCartDto payload) {

    // Create HttpEntity with the payload
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<UpdateCartDto> httpEntity = new HttpEntity<>(payload, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(cartCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("updateCart with URI: {}, payload: {}", uri, payload);

    return restTemplate.exchange(uri, PATCH, httpEntity, CartDto.class);
  }

  public ResponseEntity<CartDto> getCart(String cartCode) {
    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(cartCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getCart with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, CartDto.class);
  }

  public ResponseEntity<? extends Page<CartDto>> getAllCarts(@Nullable String userId,
      @Nullable CartType type, Pageable pageable) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .queryParamIfPresent("userId", Optional.ofNullable(userId))
        .queryParamIfPresent("type", Optional.ofNullable(type))
        .queryParam("page", pageable.getPageNumber())
        .queryParam("size", pageable.getPageSize())
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getAllCarts with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<CartDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  public ResponseEntity<? extends Page<CartDto>> getAllCarts(Pageable pageable) {
    return getAllCarts(null, null, pageable);
  }

  public ResponseEntity<Void> deleteCart(String cartCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(cartCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("deleteCart with URI: {}", uri);

    return restTemplate.exchange(uri, DELETE, null/*httpEntity*/, Void.class);
  }

}
