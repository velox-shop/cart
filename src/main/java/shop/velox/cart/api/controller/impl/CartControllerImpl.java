package shop.velox.cart.api.controller.impl;

import static org.springframework.http.HttpStatus.CREATED;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.cart.api.controller.CartController;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.model.enums.CartType;
import shop.velox.cart.service.CartService;
import shop.velox.commons.security.service.SecurityService;


@RestController
@RequiredArgsConstructor
@Slf4j
public class CartControllerImpl implements CartController {

  private final CartService cartService;

  private final SecurityService securityService;

  @Override
  public ResponseEntity<CartDto> createCart(final CreateCartDto createCartDto) {
    String currentUser = securityService.getUserId().orElse(null);
    log.info("createCart by {}", currentUser);
    CartDto createdCart = cartService.createCart(createCartDto, currentUser);
    return ResponseEntity.status(CREATED)
        .body(createdCart);
  }

  @Override
  public ResponseEntity<CartDto> getCart(final String code) {
    return cartService.getCart(code)
        .map(cartDto -> new ResponseEntity<CartDto>(cartDto, null, HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @Override
  public ResponseEntity<Void> removeCart(final String cartId) {
    return cartService.deleteCart(cartId) ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @Override
  public Page<CartDto> getCarts(@Nullable String userId, @Nullable CartType cartType,
      Pageable pageable) {
    return cartService.findAll(userId, cartType, pageable);
  }

  @Override
  public ResponseEntity<CartDto> updateCart(final String cartCode,
      final UpdateCartDto updateCartDto) {
    String userId = securityService.getUserId().orElse(null);
    log.info("user with id: {} updating cart with code: {}", userId, cartCode);
    return cartService.updateCart(cartCode, updateCartDto, userId);
  }
}
