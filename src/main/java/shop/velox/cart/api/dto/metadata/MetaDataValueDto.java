package shop.velox.cart.api.dto.metadata;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class MetaDataValueDto {

  private String stringValue;

  private BigDecimal numberValue;

}
