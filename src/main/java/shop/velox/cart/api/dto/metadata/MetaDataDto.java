package shop.velox.cart.api.dto.metadata;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class MetaDataDto {

  @Builder.Default
  List<MetaDataEntryDto> metadataEntries = new ArrayList<>();

}
