package shop.velox.cart.api.dto.cart;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.metadata.MetaDataDto;
import shop.velox.cart.model.enums.CartType;
import shop.velox.commons.security.annotation.OwnerId;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class CartDto {

  @Schema(description = "Unique identifier of the Cart.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private final String code;

  @Schema(description = "The type of the cart.", example = "SHOPPING_CART")
  @NotNull
  CartType type;

  @Schema(description = "The name of the cart.", example = "Christmas Presents")
  String name;

  @Schema(description = "Unique identifier of the User. Used for identity management.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private String userId;

  @OwnerId
  public String getUserId() {
    return userId;
  }

  @Schema(description = "The Isocode of the currency of the cart.", example = "CHF")
  @NotNull
  private String currencyIsoCode;

  @Schema(description = "List of the Items in the Cart.")
  private final List<ItemDto> items;

  @Schema(description = "Metadata of the Cart.")
  private final MetaDataDto metaData;

  public static Predicate<CartDto> anonymousCart = cartOptional -> Optional.ofNullable(cartOptional)
      .map(cart -> StringUtils.isEmpty(cart.getUserId()))
      .orElse(false);
}
