package shop.velox.cart.api.dto.item;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.cart.api.dto.metadata.MetaDataDto;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class ItemDto {

  @Schema(description = "Unique identifier of the Item.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private final String id;

  @NotBlank
  @Schema(description = "Unique identifier of the Article.", example = "article1")
  private final String productCode;

  @NotNull
  @Positive
  @Schema(description = "Quantity of article in this Item.", example = "4")
  private final BigDecimal quantity;

  @Schema(description = "Name of the Article.", example = "Pencil")
  private String name;

  @Schema(description = "Unit price of the Article.", example = "1050.25")
  private BigDecimal unitPrice;

  @Schema(description = "Total price of the Article for required quantity.", example = "4201")
  private BigDecimal totalPrice;

  @Schema(description = "Metadata of the Item.")
  private final MetaDataDto metaData;

}
