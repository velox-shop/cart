package shop.velox.cart.api.dto.cart;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.cart.api.dto.metadata.MetaDataDto;
import shop.velox.cart.model.enums.CartType;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class CreateCartDto {

  @Schema(description = "The type of the cart.", example = "SHOPPING_CART")
  @NotNull
  @Builder.Default
  CartType type = CartType.SHOPPING_CART;

  @Schema(description = "The name of the cart.", example = "Christmas Presents")
  String name;

  @Schema(description = "The Isocode of the currency of the cart.", example = "CHF")
  @NotNull
  private String currencyIsoCode;

  @Schema(description = "The MetaData of the cart.")
  MetaDataDto metaData;

}
