package shop.velox.cart.api.dto.cart;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.cart.api.dto.metadata.MetaDataDto;
import shop.velox.commons.security.annotation.OwnerId;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class UpdateCartDto {

  @Schema(description = "The name of the cart.", example = "Christmas Presents")
  String name;

  @Schema(description = "The Isocode of the currency of the cart.", example = "CHF")
  @NotNull
  private String currencyIsoCode;

  @Schema(description = "Unique identifier of the User. Used for identity management."
      , example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private String userId;

  @OwnerId
  public String getUserId() {
    return userId;
  }

  private final MetaDataDto metaData;

}
