package shop.velox.cart.api.dto.item;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.cart.api.dto.metadata.MetaDataDto;

@Data
@Builder
@Jacksonized
@FieldNameConstants
public class UpdateItemDto {

  @NotNull
  @PositiveOrZero
  @Schema(description = "Quantity of article in this Item.", example = "4")
  private final BigDecimal quantity;

  @Schema(description = "Name of the Article.", example = "Pencil")
  private String name;

  @Schema(description = "Unit price of the Article.", example = "1050.25")
  private BigDecimal unitPrice;

  @Schema(description = "Total price of the Article for required quantity.", example = "4201")
  private BigDecimal totalPrice;

  @Schema(description = "Metadata of the Item.")
  private final MetaDataDto metaData;

}
