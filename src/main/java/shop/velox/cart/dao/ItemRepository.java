package shop.velox.cart.dao;


import java.util.Optional;
import shop.velox.cart.model.ItemEntity;

public interface ItemRepository extends CustomRepository<ItemEntity, Long> {

  Optional<ItemEntity> findItemByCartCodeAndId(String cartId, String itemId);

  Optional<ItemEntity> findItemByCartCodeAndProductCode(String cartId, String productCode);

}
