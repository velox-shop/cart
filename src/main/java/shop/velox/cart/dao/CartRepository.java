package shop.velox.cart.dao;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.cart.model.CartEntity;

public interface CartRepository extends CustomRepository<CartEntity, Long> {

  Optional<CartEntity> findByCode(String code);

  Long deleteCartByCode(String code);

  Page<CartEntity> findAllByUserId(Pageable pageable, String userId);

  Page<CartEntity> findAll(Pageable pageable);

}
