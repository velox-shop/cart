package shop.velox.cart.dao;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * This adds the Entity refresh functionality to a JpaRepository
 *
 * @param <T>  the type of the entity to handle
 * @param <ID> the type of the entity's identifier
 */
@NoRepositoryBean
public interface CustomRepository<T, ID extends Serializable> extends JpaRepository<T, ID>,
    JpaSpecificationExecutor<T> {

  void refresh(T t);
}
