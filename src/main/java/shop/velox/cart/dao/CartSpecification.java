package shop.velox.cart.dao;

import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.CartEntity.Fields;
import shop.velox.cart.model.enums.CartType;

@UtilityClass
public class CartSpecification {

  public static Specification<CartEntity> isOfType(@Nullable CartType type) {
    return (root, query, builder) -> {
      if (type == null) {
        return builder.isTrue(builder.literal(true)); // always true = no filtering
      } else {
        return builder.equal(root.get(Fields.type), type);
      }
    };
  }

  public static Specification<CartEntity> belongsToUser(@Nullable String userId) {
    return (root, query, builder) -> {
      if (userId == null) {
        return builder.isTrue(builder.literal(true)); // always true = no filtering
      } else {
        return builder.equal(root.get(Fields.userId), userId);
      }
    };
  }

}
