package shop.velox.cart.dao;

import shop.velox.cart.model.MetadataEntryEntity;

public interface MetadataEntryRepository extends CustomRepository<MetadataEntryEntity, Long> {

}
