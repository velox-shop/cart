package shop.velox.cart.converter;

import org.mapstruct.Mapper;
import shop.velox.cart.api.dto.metadata.MetaDataDto;
import shop.velox.cart.api.dto.metadata.MetaDataEntryDto;
import shop.velox.cart.api.dto.metadata.MetaDataValueDto;
import shop.velox.cart.model.MetaDataEntity;
import shop.velox.cart.model.MetadataEntryEntity;
import shop.velox.cart.model.MetadataValueEntity;

@Mapper
public interface MetaDataConverter {

  MetaDataDto convertToDto(MetaDataEntity entity);

  MetaDataEntity convertToEntity(MetaDataDto dto);


  MetaDataEntryDto convertToDto(MetadataEntryEntity entity);

  MetadataEntryEntity convertToEntity(MetaDataEntryDto dto);


  MetaDataValueDto convertToDto(MetadataValueEntity entity);

  MetadataValueEntity convertToEntity(MetaDataValueDto dto);

}
