package shop.velox.cart.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;

@Mapper(uses = {
    MetaDataConverter.class,
})
public interface CreateItemConverter {

  ItemEntity convertDtoToEntity(CreateItemDto dto, @Context CartEntity cart);

  @AfterMapping
  default void fillCart(
      @MappingTarget ItemEntity.ItemEntityBuilder itemEntityBuilder,
      @Context CartEntity cart) {
    itemEntityBuilder.cart(cart);
  }

}
