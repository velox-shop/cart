package shop.velox.cart.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.model.CartEntity;

@Mapper(uses = {
    MetaDataConverter.class,
})
public interface CreateCartConverter {

  @Mappings({
      @Mapping(target = "userId", ignore = true),
  })
  CartEntity convertDtoToEntity(CreateCartDto dto, @Context String userId);

  @AfterMapping
  default void fillUserId(@MappingTarget CartEntity.CartEntityBuilder cartEntityBuilder,
      @Context String userId) {
    cartEntityBuilder.userId(userId);
  }

}
