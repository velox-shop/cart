package shop.velox.cart.converter;

import org.mapstruct.Mapper;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.commons.converter.Converter;

@Mapper(uses = ItemConverter.class)
public interface CartConverter extends Converter<CartEntity, CartDto> {

}
