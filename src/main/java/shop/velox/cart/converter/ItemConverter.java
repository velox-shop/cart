package shop.velox.cart.converter;

import org.mapstruct.Mapper;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.model.ItemEntity;
import shop.velox.commons.converter.Converter;

@Mapper
public interface ItemConverter extends Converter<ItemEntity, ItemDto> {

}
