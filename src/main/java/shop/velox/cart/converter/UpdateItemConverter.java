package shop.velox.cart.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import shop.velox.cart.api.dto.item.UpdateItemDto;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    uses = {
        MetaDataConverter.class,
    })
public interface UpdateItemConverter {

  ItemEntity convertDtoToEntity(UpdateItemDto dto, @Context CartEntity cart);

  @InheritConfiguration
  void updateEntityFromDto(UpdateItemDto dto, @MappingTarget ItemEntity itemEntity);

  @AfterMapping
  default void fillCart(
      @MappingTarget ItemEntity.ItemEntityBuilder itemEntityBuilder,
      @Context CartEntity cart) {
    itemEntityBuilder.cart(cart);
  }

}
