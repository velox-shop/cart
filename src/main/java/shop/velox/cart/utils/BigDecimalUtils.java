package shop.velox.cart.utils;

import java.math.BigDecimal;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BigDecimalUtils {

  public static boolean isPositive(BigDecimal bd) {
    return bd != null && bd.compareTo(BigDecimal.ZERO) > 0;
  }

}
