package shop.velox.cart;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

/**
 * Needed until https://github.com/spring-projects/spring-boot/issues/4779 is fixed
 */
@Slf4j
@RequiredArgsConstructor
public class RetryableDataSource extends AbstractDataSource {

  private final DataSource delegate;

  @Override
  @Retryable(maxAttempts = 10, backoff = @Backoff(multiplier = 2.3, maxDelay = 30000))
  public Connection getConnection() throws SQLException {
    return delegate.getConnection();
  }

  @Override
  @Retryable(maxAttempts = 10, backoff = @Backoff(multiplier = 2.3, maxDelay = 30000))
  public Connection getConnection(final String username, final String password)
      throws SQLException {
    return delegate.getConnection(username, password);
  }
}
