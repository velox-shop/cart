package shop.velox.cart.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import lombok.AllArgsConstructor;
import lombok.Builder.Default;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import org.apache.commons.lang3.StringUtils;
import shop.velox.cart.model.enums.CartType;
import shop.velox.commons.model.AbstractEntity;
import shop.velox.commons.security.annotation.OwnerId;

@Entity
@Table(name = "CARTS")
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldNameConstants
public class CartEntity extends AbstractEntity {

  @Column(name = "CODE", unique = true, nullable = false)
  @Default
  private String code = UUID.randomUUID().toString();

  @Column(name = "TYPE", nullable = false)
  @Default
  private CartType type = CartType.SHOPPING_CART;

  @Column(name = "NAME")
  private String name;

  @OneToMany(mappedBy = ItemEntity.Fields.cart, cascade = CascadeType.REMOVE)
  @NotNull
  @Default
  private List<ItemEntity> items = new ArrayList<>();

  @Column(name = "DIRTY")
  @Default
  private boolean dirty = false;

  @Column(name = "USER_ID")
  private String userId;

  @Column(name = "CURRENCY_ISO_CODE")
  private String currencyIsoCode;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  private MetaDataEntity metaData;

  public boolean addItem(final ItemEntity item) {
    return this.items.add(item);
  }

  @OwnerId
  public String getUserId() {
    return userId;
  }

  public static Predicate<CartEntity> anonymousCart = cartEntity -> Optional.ofNullable(cartEntity)
      .map(cart -> StringUtils.isEmpty(cart.getUserId()))
      .orElse(false);
}
