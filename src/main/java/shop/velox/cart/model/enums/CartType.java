package shop.velox.cart.model.enums;

public enum CartType {
  SHOPPING_CART,
  WISHLIST
}
