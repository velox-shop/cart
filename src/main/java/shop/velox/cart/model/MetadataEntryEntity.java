package shop.velox.cart.model;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
@FieldNameConstants
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class MetadataEntryEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "meta_key")
  private String key;

  @ElementCollection
  @CollectionTable(
      name = "metadata", // Same table as parent
      joinColumns = @JoinColumn(name = "metadata_id")
  )
  private List<MetadataValueEntity> values;
}
