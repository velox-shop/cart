package shop.velox.cart.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.io.Serial;
import java.math.BigDecimal;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;
import shop.velox.commons.model.AbstractEntity;


@Entity
@Table(name = "ITEMS")
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true)
@Jacksonized
@FieldNameConstants
@NoArgsConstructor
public class ItemEntity extends AbstractEntity {

  @Serial
  private static final long serialVersionUID = -1230190190208576850L;

  @Column(name = "ID", nullable = false, unique = true)
  @Builder.Default
  private String id = UUID.randomUUID().toString();

  @Column(name = "ARTICLE_ID", nullable = false)
  private String productCode;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "CART_PK")
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  private CartEntity cart;

  @Column(name = "QUANTITY", nullable = false)
  private BigDecimal quantity;

  @Column(name = "NAME")
  private String name;

  @Column(name = "UNIT_PRICE")
  private BigDecimal unitPrice;

  @Column(name = "TOTAL_PRICE")
  private BigDecimal totalPrice;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  private MetaDataEntity metaData;

}
