package shop.velox.cart.permissions;

import static shop.velox.cart.service.CartServiceConstants.Authorities.CART_ADMIN;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.service.CartService;
import shop.velox.commons.security.service.AuthorizationEvaluator;

@Component
@RequiredArgsConstructor
@Slf4j
public class CartAuthorizationEvaluator {

  private final AuthorizationEvaluator veloxAuthorizationEvaluator;

  private final CartService cartService;

  private final CartRepository cartRepository;

  public boolean canAccessCart(Authentication authentication, boolean isAnonymous,
      Optional<CartDto> cartOptional) {
    Boolean result = cartOptional.map(
            cartDto ->
                (isAnonymous && CartDto.anonymousCart.test(cartDto))
                    || isCurrentUser(authentication, cartDto.getUserId())
                    || hasAdminAuthority(authentication))
        // if there is no cart, we want 404, not 403
        .orElse(true);
    log.debug("Checking if user with authentication: {} can access cart: {} returns: {}",
        authentication, cartOptional.map(CartDto::getCode), result);
    return result;
  }

  public boolean canAccessCartWithCode(Authentication authentication, boolean isAnonymous,
      String cartCode) {
    boolean result = hasAdminAuthority(authentication) || canAccessCart(authentication, isAnonymous,
        cartService.getCart(cartCode));
    log.debug("Checking if user with authentication: {} can access cart with code: {} returns: {}",
        authentication, cartCode, result);
    return result;
  }

  public boolean isCurrentUser(Authentication authentication, String externalId) {
    boolean result = veloxAuthorizationEvaluator.isCurrentUserId(authentication, externalId);
    log.debug(
        "Checking if user with authentication: {} is current user with externalId: {} returns: {}",
        authentication, externalId, result);
    return result;
  }

  public boolean hasAdminAuthority(Authentication authentication) {
    boolean result = veloxAuthorizationEvaluator.hasGlobalOrCustomAdminAuthority(
        authentication, CART_ADMIN);
    log.debug("Checking if user with authentication: {} has admin authority returns: {}",
        authentication,
        result);
    return result;
  }

  public boolean canAccessUserCarts(Authentication authentication, String externalId) {
    boolean isCurrentUser = isCurrentUser(authentication, externalId);
    boolean hasAdminAuthority = hasAdminAuthority(authentication);
    boolean result = isCurrentUser || hasAdminAuthority;
    log.debug(
        "Checking if user with authentication: {} can access carts of user externalId: {} returns: {}",
        authentication, externalId, result);
    return result;
  }

  public boolean canEditCart(Authentication authentication, String cartCode, boolean isAnonymous) {
    // This checks case where user takes over anonymous cart
    boolean result =
        cartRepository.findByCode(cartCode).filter(c -> CartEntity.anonymousCart.test(c))
            .isPresent()
            || canAccessCartWithCode(authentication, isAnonymous, cartCode);
    log.debug("Checking if user with authentication: {} can edit cart with code: {} returns: {}",
        authentication, cartCode, result);
    return result;

  }


}
