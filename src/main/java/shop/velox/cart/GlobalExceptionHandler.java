package shop.velox.cart;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)  // 422
  @ExceptionHandler(BindException.class)
  public void handleIllegalArgumentException(BindException e) {
    log.debug("{}", e.getAllErrors());
  }

  @ResponseStatus(HttpStatus.CONFLICT)  // 409
  @ExceptionHandler(DataIntegrityViolationException.class)
  public void handleIllegalArgumentException(DataIntegrityViolationException e) {
    log.debug(e.getMessage(), e);
  }
}
