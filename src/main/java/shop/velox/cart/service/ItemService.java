package shop.velox.cart.service;

import java.util.Optional;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;

public interface ItemService {

  /**
   * Gets an item by cart id and item id
   *
   * @param cartCode the id of the cart
   * @param itemId   the id of the item
   * @return the item wrapped in a {@link Optional}
   */
  @PreAuthorize("@cartAuthorizationEvaluator.canAccessCartWithCode(authentication, isAnonymous(), #cartCode)")
  Optional<ItemDto> getItem(@Param("cartCode") String cartCode, @Param("itemId") String itemId);

  /**
   * Creates an item in the cart with code cartCode, with given productCode and quantity
   *
   * @param cartCode the id of the cart
   * @param item     contains the information related to the item
   * @return the updated cart
   */
  @PreAuthorize("@cartAuthorizationEvaluator.canAccessCartWithCode(authentication, isAnonymous(), #cartCode)")
  ResponseEntity<CartDto> addItem(@Param("cartCode") String cartCode, CreateItemDto item);

  /**
   * Updates an item in the cart with code cartCode
   *
   * @param cartCode the id of the cart
   * @param item     contains the information related to the item
   * @return the updated cart
   */
  @PreAuthorize("@cartAuthorizationEvaluator.canAccessCartWithCode(authentication, isAnonymous(), #cartCode)")
  ResponseEntity<CartDto> updateItem(@Param("cartCode") String cartCode, String itemId,
      UpdateItemDto item);

  /**
   * Removes an item with given id from cart with given id
   *
   * @param cartCode the id of the cart
   * @param itemId   the id of the item
   * @return the updated cart
   */
  @PreAuthorize("@cartAuthorizationEvaluator.canAccessCartWithCode(authentication, isAnonymous(), #cartCode)")
  ResponseEntity<CartDto> removeItem(@Param("cartCode") String cartCode,
      @Param("itemId") String itemId);

}
