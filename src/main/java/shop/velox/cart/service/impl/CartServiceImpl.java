package shop.velox.cart.service.impl;

import static shop.velox.cart.dao.CartSpecification.belongsToUser;
import static shop.velox.cart.dao.CartSpecification.isOfType;

import jakarta.transaction.Transactional;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.converter.CartConverter;
import shop.velox.cart.converter.CreateCartConverter;
import shop.velox.cart.converter.MetaDataConverter;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.dao.ItemRepository;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.enums.CartType;
import shop.velox.cart.service.CartService;

@Service("cartService")
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {

  private final CartRepository cartRepository;
  private final ItemRepository itemRepository;

  private final CartConverter cartConverter;
  private final CreateCartConverter createCartConverter;
  private final MetaDataConverter metaDataConverter;

  @Override
  public CartDto createCart(CreateCartDto createCartDto, String userId) {
    CartEntity newCartEntity = cartRepository.saveAndFlush(
        createCartConverter.convertDtoToEntity(createCartDto, userId));
    CartDto newCartDto = cartConverter.convertEntityToDto(newCartEntity);
    log.debug("Created cart: {}", newCartDto);
    return newCartDto;
  }

  @Override
  public Optional<CartDto> getCart(final String code) {
    return cartRepository.findByCode(code)
        .map(cartConverter::convertEntityToDto);
  }

  @Override
  @Transactional
  public boolean deleteCart(final String cartCode) {
    return cartRepository.deleteCartByCode(cartCode) > 0;
  }

  @Override
  public Page<CartDto> findAll(@Nullable String userId, @Nullable CartType cartType,
      Pageable pageable) {
    return cartRepository.findAll(belongsToUser(userId).and(isOfType(cartType)), pageable)
        .map(cartConverter::convertEntityToDto);
  }

  @Override
  public ResponseEntity<CartDto> updateCart(final String cartCode,
      final UpdateCartDto updateCartDto, final String userId) {
    final Optional<CartEntity> cart = cartRepository.findByCode(cartCode);
    if (cart.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    cart.get().setUserId(userId);

    String currencyIsoCode = updateCartDto.getCurrencyIsoCode();
    if (StringUtils.isNotBlank(currencyIsoCode)) {
      cart.get().setCurrencyIsoCode(currencyIsoCode);
    }

    if (updateCartDto.getMetaData() != null) {
      cart.get().setMetaData(metaDataConverter.convertToEntity(updateCartDto.getMetaData()));
    }

    var updatedCart = cartRepository.saveAndFlush(cart.get());
    return new ResponseEntity<>(cartConverter.convertEntityToDto(updatedCart), null,
        HttpStatus.OK);
  }

}
