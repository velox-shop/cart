package shop.velox.cart.service.impl;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.net.URI;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import shop.velox.cart.api.controller.ItemController;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.item.CreateItemDto;
import shop.velox.cart.api.dto.item.ItemDto;
import shop.velox.cart.api.dto.item.UpdateItemDto;
import shop.velox.cart.converter.CartConverter;
import shop.velox.cart.converter.CreateItemConverter;
import shop.velox.cart.converter.ItemConverter;
import shop.velox.cart.converter.UpdateItemConverter;
import shop.velox.cart.dao.CartRepository;
import shop.velox.cart.dao.ItemRepository;
import shop.velox.cart.model.CartEntity;
import shop.velox.cart.model.ItemEntity;
import shop.velox.cart.service.ItemService;
import shop.velox.cart.utils.BigDecimalUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class ItemServiceImpl implements ItemService {

  private final CartRepository cartRepository;

  private final ItemRepository itemRepository;

  private final CartConverter cartConverter;

  private final ItemConverter itemConverter;

  private final CreateItemConverter createItemConverter;

  private final UpdateItemConverter updateItemConverter;

  @Override
  public Optional<ItemDto> getItem(final String cartCode, final String itemId) {
    return itemRepository.findItemByCartCodeAndId(cartCode, itemId)
        .map(itemConverter::convertEntityToDto);
  }

  @Override
  @Transactional
  public ResponseEntity<CartDto> addItem(final String cartCode, final CreateItemDto item) {

    Optional<CartEntity> cartOptional = cartRepository.findByCode(cartCode);
    if (cartOptional.isEmpty()) {
      return new ResponseEntity<>(NOT_FOUND);
    }
    CartEntity cart = cartOptional.get();

    Optional<ItemEntity> existingItem = itemRepository
        .findItemByCartCodeAndProductCode(cartCode, item.getProductCode());
    if (existingItem.isPresent()) {
      HttpHeaders headers = new HttpHeaders();
      String existingItemId = existingItem.get().getId();
      URI existingItemLocation = linkTo(ItemController.class, cartCode)
          .slash(existingItemId)
          .toUri();
      headers.setLocation(existingItemLocation);
      return ResponseEntity.status(HttpStatus.CONFLICT)
          .headers(headers)
          .build();
    }

    ItemEntity itemToCreate = createItemConverter.convertDtoToEntity(item, cart);
    ItemEntity createdItem = itemRepository.saveAndFlush(itemToCreate);

    cart.addItem(createdItem);
    CartEntity updatedCart = cartRepository.saveAndFlush(cart);
    CartDto updatedCartDto = cartConverter.convertEntityToDto(updatedCart);

    return ResponseEntity.status(HttpStatus.CREATED)
        .body(updatedCartDto);
  }

  @Override
  public ResponseEntity<CartDto> updateItem(final String cartCode, String itemId,
      final UpdateItemDto item) {

    CartEntity cart = cartRepository.findByCode(cartCode)
        .orElseThrow(() -> new HttpClientErrorException(NOT_FOUND));

    ItemEntity existingItem = itemRepository.findItemByCartCodeAndId(cartCode, itemId)
        .orElseThrow(() -> new HttpClientErrorException(NOT_FOUND));

    updateItemConverter.updateEntityFromDto(item, existingItem);

    if (BigDecimalUtils.isPositive(existingItem.getQuantity())) {
      itemRepository.saveAndFlush(existingItem);
    } else {
      removeItem(cart.getCode(), itemId);
    }

    var updatedCart = cartRepository.findByCode(cartCode)
        .map(cartConverter::convertEntityToDto)
        .orElseThrow(() -> new HttpClientErrorException(NOT_FOUND));

    return ResponseEntity.ok(updatedCart);
  }

  @Override
  public ResponseEntity<CartDto> removeItem(final String cartCode, final String itemId) {
    Optional<CartEntity> cartBeforeRemoval = cartRepository.findByCode(cartCode);
    if (cartBeforeRemoval.isEmpty()) {
      return new ResponseEntity<>(NOT_FOUND);
    }
    boolean removed = removeItem(cartBeforeRemoval.get(), itemId);
    if (!removed) {
      return new ResponseEntity<>(NOT_FOUND);
    }

    Optional<CartEntity> cartEntity = cartRepository.findByCode(cartCode);
    return cartEntity.map(
            entity -> new ResponseEntity<>(cartConverter.convertEntityToDto(entity), null,
                HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(NOT_FOUND));
  }

  @Transactional
  boolean removeItem(final CartEntity cart, final String itemId) {
    Optional<ItemEntity> itemToRemove = cart.getItems().stream()
        .filter(itemEntity -> itemId.equals(itemEntity.getId()))
        .findAny();

    if (itemToRemove.isEmpty()) {
      return false;
    }

    itemRepository.delete(itemToRemove.get());
    cartRepository.refresh(cart);
    return true;
  }


}
