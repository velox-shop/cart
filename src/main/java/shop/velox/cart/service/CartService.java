package shop.velox.cart.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import shop.velox.cart.api.dto.cart.CartDto;
import shop.velox.cart.api.dto.cart.CreateCartDto;
import shop.velox.cart.api.dto.cart.UpdateCartDto;
import shop.velox.cart.model.enums.CartType;

/**
 * Executes CRUD operations on Cart and Item
 */
public interface CartService {

  /**
   * Creates a cart for specified user and returns it
   *
   * @param createCartDto the cart to create
   * @param userId        unique user identifier
   * @return the just created cart
   */
  @PreAuthorize("true")
  // Always allowed  to create a cart
  CartDto createCart(CreateCartDto createCartDto, String userId);

  /**
   * Gets a cart by its code
   *
   * @param cartCode the id of the cart
   * @return the cart wrapped in a {@link Optional}
   */
  @PostAuthorize("@cartAuthorizationEvaluator.canAccessCart(authentication, isAnonymous(), returnObject)")
  Optional<CartDto> getCart(String cartCode);

  /**
   * Removes a cart with given id
   *
   * @param cartCode the code of the cart
   * @return the updated cart
   */
  @PreAuthorize("@cartAuthorizationEvaluator.canAccessCartWithCode(authentication, isAnonymous(), #cartCode)")
  boolean deleteCart(@Param("cartCode") String cartCode);

  @PreAuthorize("@cartAuthorizationEvaluator.canAccessUserCarts(authentication, #userId)")
  Page<CartDto> findAll(@Nullable @Param("userId") String userId,
      @Nullable CartType cartType, Pageable pageable);

  @PreAuthorize("@cartAuthorizationEvaluator.canEditCart(authentication, #cartCode, isAnonymous())")
  ResponseEntity<CartDto> updateCart(@Param("cartCode") String cartCode,
      UpdateCartDto updateCartDto, String userId);
}
